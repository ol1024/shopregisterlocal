-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2020 at 01:04 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wnt`
--

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `uri` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `body`, `created_at`) VALUES
(1, 'Post One', 'post-one', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at fringilla elit. Quisque aliquet condimentum porta. Integer sit amet dignissim nisi, eget consectetur lacus. Nunc placerat laoreet odio, vitae ornare sapien lacinia feugiat. Aenean posuere blandit urna, eget convallis lorem placerat nec. Sed convallis ac purus id convallis. Ut venenatis arcu sit amet massa congue posuere. Mauris blandit interdum egestas. Pellentesque a varius neque, eu sodales augue. Duis libero augue, finibus non sodales et, dictum at metus.', '2020-03-28 20:18:38'),
(2, 'Post Two', 'post-two', 'Vivamus eu sapien vel orci faucibus viverra vitae nec eros. Aenean dictum elementum tellus, et auctor ligula ullamcorper quis. Morbi bibendum fermentum faucibus. Suspendisse et dictum mauris. Cras porttitor est quis nunc gravida, eget interdum justo pellentesque. Phasellus aliquam augue tincidunt neque dapibus, in varius tellus convallis. Nunc varius porta ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam tempor enim ut risus rutrum sagittis. Aenean sit amet feugiat ante. Nulla ex ex, tincidunt eu leo sed, fermentum iaculis lacus. Suspendisse quis est rutrum, dignissim tellus eget, consectetur enim. Morbi gravida in lorem ac congue. Vivamus faucibus vestibulum mi a iaculis. Nunc rhoncus lacus mi, eu posuere mauris elementum non. Etiam gravida ex risus, in porta lectus bibendum ut.', '2020-03-28 20:18:38'),
(3, 'Post Three', 'Post-Three', 'This is post number three', '2020-03-28 21:39:24');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(1000) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci DEFAULT NULL,
  `address` varchar(45) CHARACTER SET ucs2 COLLATE ucs2_unicode_ci NOT NULL,
  `tel_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry_id` int(11) NOT NULL,
  `businessReg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ig_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `or_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`id`, `name`, `desc`, `address`, `tel_no`, `industry_id`, `businessReg`, `fb_link`, `ig_link`, `or_link`, `lat`, `lng`) VALUES
(1, 'test retail', '', '駿洋邨駿逸樓', '12345678', 2, '123456', '', '', '', 22.4, 114.19),
(2, 'test retail', '', '駿洋邨駿逸樓', '12345678', 2, '234567', '', '', '', 22.4, 114.19),
(3, 'test restaurant', '', '駿洋邨駿逸樓2', '12345678', 1, '345678', '', '', '', 22.4, 114.19),
(4, 'test restaurant', '', '駿洋邨駿逸樓3', '12345678', 1, '456789', '', '', '', 22.4, 114.19),
(5, 'test restaurant', '', '駿洋邨駿逸樓5', '12345678', 1, '567890', '', '', '', 22.4, 114.19),
(6, 'test NGO', '', '金鐘添美道300號', '12345678', 3, '678901', '', '', '', 22.2799, 114.166),
(7, 'test NGO 2', '', '金鐘添美道301號', '12345678', 3, '789012', '', '', '', 22.2806, 114.167),
(8, 'test NGO 8', '', '金鐘添美道8號', '09876543', 3, '890123', '', '', '', 22.2799, 114.166),
(9, 'test 9', '', '金鐘添美道10號', '12345678', 3, '1234567', '', '', '', 22.2799, 114.166),
(10, 'test 11', '', '金鐘添美道11號', '12345678', 3, '3456789', '', '', '', 22.2799, 114.166),
(11, 'test 12', '', '金鐘添美道12號', '09876543', 2, '4567890', '', '', '', 22.2799, 114.166),
(12, 'test 13', '', '金鐘添美道123號', '12345783', 2, '12345678', '', '', '', 22.2799, 114.166),
(13, 'test 14', '', '123456', '12345678', 2, '123456789', '', '', '', 22.324, 114.166),
(14, 'test 15', '', '金鐘添美道266號', '12345678', 2, '123465780', '', '', '', 22.2799, 114.166),
(15, '123456', '', '金鐘添美道321號', '12345678', 2, '321', '', '', '', 22.2799, 114.166),
(16, 'test 18', '', '金鐘添美道18號', '09876543', 3, '', '', '', '', 22.2799, 114.166),
(17, 'test 19', '', '金鐘添美道19號', '12345678', 2, '', '', '', '', 22.2799, 114.166),
(18, 'test 20', '', '金鐘添美道20號', '12345678', 2, '', '', '', '', 22.2799, 114.166),
(19, 'test 21', '', '金鐘添美道21號', '12345678', 2, '', '', '', '', 22.2799, 114.166),
(20, 'test 22', '', '123456', '12345678', 3, '', '', '', '', 22.324, 114.166),
(21, 'test 24', '', '金鐘添美道24號', '12345678', 3, '', '', '', '', 22.2799, 114.166),
(22, 'test 25', '', '駿洋邨駿逸樓25', '12345678', 2, '', '', '', '', 22.4, 114.19),
(23, 'test 26', '', '駿洋邨駿逸樓', '13245678', 3, NULL, '', '', '', 22.4, 114.19),
(24, 'test 27', '', '金鐘添美道2號', '12345678', 2, NULL, '', '', '', 22.2805, 114.165),
(25, 'test 28', '', '金鐘添美道2號', '12345678', 2, NULL, '', '', '', 22.2805, 114.165),
(26, 'test 29', '', '金鐘添美道2號', '12345678', 2, NULL, '', '', '', 22.2805, 114.165),
(27, 'test 30', '', '金鐘添美道2號', '12345678', 2, '27_2004061215_photo_2020-02-13_14-31-08.jpg', '', '', '', 22.2805, 114.165);

-- --------------------------------------------------------

--
-- Table structure for table `stores_image`
--

CREATE TABLE `stores_image` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `store_industry`
--

CREATE TABLE `store_industry` (
  `id` int(11) NOT NULL,
  `desc` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `store_industry`
--

INSERT INTO `store_industry` (`id`, `desc`) VALUES
(1, 'Restaurant'),
(2, 'Retail'),
(3, 'NGO');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_verified` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `password`, `is_verified`) VALUES
(1, 'test@test.test', '$2y$10$nBY4gvO2UAsnC8Dq1agWseVlYvMyOpRKtNLLREYqKFCjDeNFJ4/Gq', 0),
(2, 'test2@test.test', '$2y$10$TshAkia5Y4LuipSyuOCLgud.cxA5GvS0Ve3KwY/lwUrUh4iniDSfe', 0),
(3, 'test3@test.test', '$2y$10$/Zpo967wCcqMKr1ncMkKZe0wRpOa1OsslyJeKJPAlkG.4b/.3U4zy', 0),
(4, 'test4@test.test', '$2y$10$zAm0whxeiHRqIb1L60xXQ.UWhsEDla.PgZaNidWyXL2Q1ZAbcjzWS', 0),
(5, 'test5@test.test', '$2y$10$w2fAT./nuL3IJJGoSLSI4eOwqvdQr2mYsrvKui0.i6OMYjY/m9chu', 0),
(6, 'test6@test.test', '$2y$10$aOSHAwH.khIHUh0cV9BCteFWaxNpxOgzk.Q6dU0GWFNCjcyf3ZpSe', 0),
(7, 'test7@test.test', '$2y$10$t8JoiXLRig/Z30eYGfdcNu27EFHhmVKaeyDhaBZF/Lh7p4SCwSHAG', 0),
(8, 'test8@test.test', '$2y$10$Bov7rofp8G5C4SvPeJAhm.1HPLYraM9RvJ6NjBTdNSw6UfaC5npIK', 0),
(9, 'test9@test.test', '$2y$10$nHjbN64u83gA48LPfIHXaeKfPoXdzJmXj3vhBpGONynqVZioJK2t.', 0),
(10, 'test11@test.test', '$2y$10$NZ3pFQ2WX.SO/Ytx0w1FA.G58zFzcz1LJlRMmBl2t4BnkuQNllufS', 0),
(11, 'test12@test.test', '$2y$10$FNUJnvcTZWD9kHCtSTmUyeT2KeCTxDOz5Z.j4i600dEJ4ZskpUKdG', 0),
(12, 'test13@test.test', '$2y$10$i8A7L9it1myn6q8d2PaiMeWJPw/LTOWvQgdNWKbbJ4ePN.VumgAlK', 0),
(13, 'test14@test.test', '$2y$10$hIktci8r68g3b01OqqTEAeYrYH0/I8rF4SO2.rbuh2No1wbEUbBBC', 0),
(14, 'test15@test.test', '$2y$10$Yn59G9tqdOyQcAuC5cpzfuVdvET.1o6H1qXBh.25uozlgcIKf3TKq', 0),
(15, 'test16@test.test', '$2y$10$5u6m/bnINM0jgXQvxmUpGOhVIMyagmZOu8hghT/ZTqEon04lFizVa', 1),
(16, 'test18@test.test', '$2y$10$DBjONzh5G6qHFwfpiZy1..H9yLvBGf5/Bhd969yn16Er6mkalr2BK', 0),
(17, 'test19@test.test', '$2y$10$RjWsMzPXRFx0rZZ/7lQE4ut6eJaP4xQHu15KOoFGT.RqCj1mIB616', 0),
(18, 'test20@test.test', '$2y$10$bem3eSqnjXSR.ZMqJ6CSCO9hO3OPbY1w1tVOVl/M7NXUNkuIIKaZ2', 0),
(19, 'test21@test.test', '$2y$10$dxV8PFZuPOWNkAeilqTkkuzwu.KY35pcNkQz7dLBlgRUn/hR4TlPa', 0),
(20, 'test22@test.test', '$2y$10$40ZoayDMnTZ3.RrD7gIqKeBhgYsANVlHwAzTqlPWCShyCIqCd2sJq', 0),
(21, 'test24@test.test', '$2y$10$Wy2memza4a9c7MDEAB9jNe5poEPzBnds83hXCuyTfJYkJUF9VmChi', 0),
(22, 'test25@test.test', '$2y$10$WhWH9vwPBO6ePXqnqCW3qO.yqhgo7pnFtKY6.Hq3SHhKsKoHbVDAW', 0),
(23, 'test26@test.test', '$2y$10$VG2g3LzEGUfgOeIA/xqAEOKqK6C4Yl9ziPVD1/S90MySms6UkkFee', 0),
(24, 'test27@test.test', '$2y$10$Ypu..1muTLAyJ1ixCsB5WuNg6zQKxxjXrTkEedUO8JDGf0iwB9GOC', 0),
(25, 'test28@test.test', '$2y$10$m9qivddR03na.imjsunyYO0p0zigPlGCNQNDT1ycR1MBnWil6wF16', 0),
(26, 'test29@test.test', '$2y$10$hPxe4TbYbjBtUUQJr0qK9.QyCJcwzhfDmQcUTII4OMfnPzxMSfA9q', 0),
(27, 'test30@test.test', '$2y$10$p42KP2YvPIXF5saFF17hIeE.iagG8wl/qAfV.T8c6qzc7iT2arXEG', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_store`
--

CREATE TABLE `user_store` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `is_verified` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_store`
--

INSERT INTO `user_store` (`id`, `user_id`, `store_id`, `is_verified`) VALUES
(1, 1, 1, 0),
(2, 2, 2, 0),
(3, 3, 3, 0),
(4, 4, 4, 0),
(5, 5, 5, 0),
(6, 6, 6, 0),
(7, 7, 7, 0),
(8, 8, 8, 0),
(9, 9, 9, 0),
(10, 10, 10, 0),
(11, 11, 11, 0),
(12, 12, 12, 0),
(13, 13, 13, 0),
(14, 14, 14, 0),
(15, 15, 15, 0),
(16, 16, 16, 0),
(17, 17, 16, 0),
(18, 18, 16, 0),
(19, 19, 16, 0),
(20, 20, 16, 0),
(21, 21, 16, 0),
(22, 22, 16, 0),
(23, 23, 23, 0),
(24, 24, 23, 0),
(25, 25, 23, 0),
(26, 26, 23, 0),
(27, 27, 27, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores_image`
--
ALTER TABLE `stores_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `store_industry`
--
ALTER TABLE `store_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_store`
--
ALTER TABLE `user_store`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `store`
--
ALTER TABLE `store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `stores_image`
--
ALTER TABLE `stores_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `store_industry`
--
ALTER TABLE `store_industry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user_store`
--
ALTER TABLE `user_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
