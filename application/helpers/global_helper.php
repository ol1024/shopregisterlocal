<?php
    function debug($val, $stop = true, $return = false) {
        $rt = false;
        $c = print_r($val, true);

        if ($return) {
            $rt = $c;
        } else {
            echo "<pre>";
            print_r($val);
            echo "</pre>";
        }

        if ($stop)
            exit();

        return $rt;
    }

