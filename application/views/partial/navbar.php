<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary" style="min-height:70px;">
        <a class="navbar-brand" href="<?php echo base_url();?>" style="width:70px;"><img class="logo" src="<?php echo base_url();?>assets/images/WNT_logo.png" alt="wnt" style="margin-top:10px;height:50px;width:50px;"></a>
        <?php if($this->session->userdata('logged_in')==true): ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="true" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarResponsive">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url().'home'; ?>">主頁</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url().'editPassword'; ?>">更改密碼</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?= base_url().'logout'; ?>">登出</a>
            </li>
          </ul>
        </div>
        <?php endif; ?>
    </nav>

    <div class="container">
