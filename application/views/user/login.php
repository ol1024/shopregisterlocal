<h2>和你推商戶登入</h2>
<br>
<br>
<?php
  if(form_error('email')) echo form_error('email','<div class="validError">','</div>'); 
  else echo form_error('password','<div class="validError">','</div>')
?>

<?php echo form_open('login'); ?>
<?php echo form_fieldset('帳戶登入');?>
  <div class="form-group">
    <label class="required">電郵地址</label>
    <input type="text" class="form-control" name="email" aria-describedby="emailHelp" placeholder="電郵地址">
  </div>
  <div class="form-group">
    <label class="required">密碼</label>
    <input type="password" class="form-control" name="password" aria-describedby="PasswordHelp" placeholder="密碼">
  </div>
  <?php echo form_fieldset_close(); ?>

  <button type="button" onclick="window.location='<?php echo base_url(); ?>'" class="btn btn-secondary">取消</button>
  <button type="submit" class="btn btn-primary">登入</button>
</form>

<div style="padding-top:15px;">還未註冊帳戶？<a href="<?php echo base_url()."registration"; ?>">請按此註冊</div>
