<h2>和你推商戶登記</h2>

<?php echo form_open('registration/validate', array('id'=>'regForm',  'enctype'=>'multipart/form-data')); ?>
  <?php echo form_fieldset('帳戶資料');?>
  <div class="form-group">
    <label class="required">電郵地址</label>
    <input type="text" class="form-control<?=(form_error('email'))?' reenter':'';?>" name="email" aria-describedby="emailHelp" placeholder="電郵地址" value="<?=$email;?>">
    <?php echo form_error('email','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label class="required">密碼</label>
    <input type="password" class="form-control<?=($error==1)?' reenter':'';?>" name="password" aria-describedby="PasswordHelp" placeholder="密碼">
    <?php echo form_error('password','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label class="required">確認密碼</label>
    <input type="password" class="form-control<?=($error==1)?' reenter':'';?>" name="passwordConfirm" aria-describedby="PasswordComfirmHelp" placeholder="確認密碼">
    <?php echo form_error('passwordConfirm','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <?php echo form_fieldset_close(); ?>

  <?php echo form_fieldset('商戶資料');?>
  <div>
    <label class="required">商戶種類</label>
    <div>
      <input type="radio"<?=(form_error('industry'))?' class="reenter"':'';?> id="shop_industry_1" name="industry" value="1"<?php echo ($industry=='1')?' checked':''; ?>>
      <label for="shop_industry_1">餐廳</label>
      <input type="radio"<?=(form_error('industry'))?' class="reenter"':'';?> id="shop_industry_2" name="industry" value="2"<?php echo ($industry=='2')?' checked':''; ?>>
      <label for="shop_industry_2">零售</label>
      <input type="radio"<?=(form_error('industry'))?' class="reenter"':'';?> id="shop_industry_3" name="industry" value="3"<?php echo ($industry=='3')?' checked':''; ?>>
      <label for="shop_industry_3">非牟利機構</label>
      <?php echo form_error('industry','<div class="validError">','</div><br>'); ?>
    </div>
  </div>
  <div class="form-group">
    <label class="required">商戶名稱</label>
    <input type="text" class="form-control<?=(form_error('name'))?' reenter':'';?>" name="name" aria-describedby="nameHelp" placeholder="商戶名稱" value="<?=$name;?>">
    <?php echo form_error('name','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label class="required">商業登記證副本（上限2MB，只限於 jpg / jpeg / png ）</label>
    <!-- <input type="text" class="form-control" name="businessReg" aria-describedby="businessRegNumberHelp" placeholder="business registration number"> -->
    <input type="file" class="form-control<?=($error==1)?' reenter':'';?>" name="businessReg" aria-describedby="businessRegNumberHelp">
    <?php echo form_error('businessReg','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label class="required">商戶地址</label>
    <input type="text" class="form-control<?=(form_error('address'))?' reenter':'';?>" name="address" aria-describedby="AddressHelp" placeholder="商戶地址" value="<?=$address;?>">
    <?php echo form_error('address','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label class="required">聯絡電話</label>
    <input type="text" class="form-control<?=(form_error('telno'))?' reenter':'';?>" name="telno" aria-describedby="ContactNumberHelp" placeholder="聯絡電話" value="<?=$telno;?>">
    <?php echo form_error('telno','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label class="required">商戶大頭照（上限2MB，只限於 jpg / jpeg / png ）</label>
    <input type="file" class="form-control<?=($error==1)?' reenter':'';?>" name="businessIcon" aria-describedby="businessIconHelp">
    <?php echo form_error('businessIcon','<div class="validError"><br>','<br></div>'); ?>
  </div>
  <div class="form-group">
    <label>商戶簡介</label>
    <textarea class="form-control" name="desc" aria-describedby="DescriptionHelp" placeholder="商戶簡介" value="<?=$desc;?>"></textarea>
  </div>
  <div class="form-group">
    <label>Facebook 網址</label>
    <input type="text" class="form-control" name="fb_link" aria-describedby="FacebookHelp" placeholder="Facebook 網址" value="<?=$fb_link;?>">
  </div>
  <div class="form-group">
    <label>Instagram 網址</label>
    <input type="text" class="form-control" name="ig_link" aria-describedby="InstagramHelp" placeholder="Instagram 網址" value="<?=$ig_link;?>">
  </div>
  <div class="form-group">
    <label>Openrice 網址</label>
    <input type="text" class="form-control" name="or_link" aria-describedby="OrHelp" placeholder="Openrice 網址" value="<?=$or_link;?>">
  </div>
  <?php echo form_fieldset_close(); ?>
  <input type="hidden" class="form-control" name="success" id="success" value="">

  <button type="button" class="btn btn-secondary" onclick="location.href='<?= base_url(); ?>'">取消</button>
  <button type="submit" class="btn btn-primary">提交登記</button>
</form>
<div style="padding-top:15px;">已註冊帳戶？<a href="<?php echo base_url()."login"; ?>">請按此登入</div>
<script>
    $(document).ready(function(){
        window.history.replaceState('', '', '/shopregister/registration');
    });
</script>