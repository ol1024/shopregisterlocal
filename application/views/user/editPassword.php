<?php echo form_open('editPassword/validate'); ?>
<?php echo form_fieldset('更改密碼');?>
  <br>
  <div class="form-group">
    <label class="required">現正使用密碼</label>
    <input type="password" class="form-control" name="oldpassword" aria-describedby="currentPasswordHelp" placeholder="現正使用密碼">
  </div>
  <?php echo form_error('oldpassword','<div class="validError">','</div><br>')?>
  <div class="form-group">
    <label class="required">新密碼</label>
    <input type="password" class="form-control" name="newpassword" aria-describedby="newPasswordHelp" placeholder="新密碼">
  </div>
  <?php echo form_error('newpassword','<div class="validError">','</div><br>')?>
  <div class="form-group">
    <label class="required">確認新密碼</label>
    <input type="password" class="form-control" name="newpasswordConfirm" aria-describedby="newPasswordConfirmHelp" placeholder="確認新密碼">
  </div>
  <?php echo form_error('newpasswordConfirm','<div class="validError">','</div><br>')?>
  <?php echo form_fieldset_close(); ?>

  <button type="button" onclick="window.location='<?php echo base_url(); ?>'" class="btn btn-secondary">取消</button>
  <button type="submit" class="btn btn-primary">更改</button>
</form>