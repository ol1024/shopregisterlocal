<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/user_home_card.css">

<!-- <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.10.2/jquery-ui.min.js"></script>
<script src="<?php echo base_url(); ?>assets/library/jQuery_tagEditor/jquery.caret.min.js"></script>
<script src="<?php echo base_url(); ?>assets/library/jQuery_tagEditor/jquery.tag-editor.min.js"></script> -->
<?php if($msg) echo "<p>$msg</p><br>"; ?>

<?php foreach($data as $k => $v): ?>
<div class="outer_layer col-12 col-sm-7 col-md-5 col-lg-4 col-xl-4">
    <div class="MuiCardContent-root">
        <div class="busIconDiv">
            <img class="busIcon" src="<?php echo file_exists($v['icon'])?base_url().$v['icon']:base_url().'assets/images/WNT_logo.png'; ?>" alt="#Sean Cafe">
        </div>
        <p class="industryDesc"><?=$v['industry']?></p>
        <h3 class="card_h3"><?=$v['name']?></h3>
        <div class="MuiGrid-container">
            <?php if($v['tel_no']):?>
            <div class="row detailRow">
            <div class="col-2" style="padding-right:0;float:right;">
                <svg class="detailIcon infoIcon" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                    <path d="M6.54 5c.06.89.21 1.76.45 2.59l-1.2 1.2c-.41-1.2-.67-2.47-.76-3.79h1.51m9.86 12.02c.85.24 1.72.39 2.6.45v1.49c-1.32-.09-2.59-.35-3.8-.75l1.2-1.19M7.5 3H4c-.55 0-1 .45-1 1 0 9.39 7.61 17 17 17 .55 0 1-.45 1-1v-3.49c0-.55-.45-1-1-1-1.24 0-2.45-.2-3.57-.57-.1-.04-.21-.05-.31-.05-.26 0-.51.1-.71.29l-2.2 2.2c-2.83-1.45-5.15-3.76-6.59-6.59l2.2-2.2c.28-.28.36-.67.25-1.02C8.7 6.45 8.5 5.25 8.5 4c0-.55-.45-1-1-1z"></path>
                </svg>
            </div>
            <div class="col-10" style="padding-left:10px;"><?=$v['tel_no']?></div>
            </div>
            <?php endif; ?>
            <?php if($v['address']):?>
            <div class="row detailRow">
            <div class="col-2" style="padding-right:0;float:right;">
                <svg class="detailIcon infoIcon" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                    <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
                </svg>
            </div>
            <div class="col-10" style="padding-left:10px;"><?=$v['address']?></div>
            </div>
            <?php endif; ?>
            <?php
                $width = ($v['fb_link'])?70:0;
                $width += ($v['ig_link'])?70:0;
                $width += ($v['or_link'])?70:0;
            ?>
            <div style="margin:auto;width:<?=$width?>px;margin-bottom:50px;margin-top:40px;">
                <?php if($v['fb_link']): ?>
                <a href="<?=$v['fb_link']?>" target="_blank">
                <svg class="shareIcon" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                    <path d="M5 3h14a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2m13 2h-2.5A3.5 3.5 0 0 0 12 8.5V11h-2v3h2v7h3v-7h3v-3h-3V9a1 1 0 0 1 1-1h2V5z"></path>
                </svg></a>
                <?php endif; ?>
                <?php if($v['ig_link']): ?>
                <a href="<?=$v['ig_link']?>" target="_blank">
                <svg class="shareIcon" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation">
                    <path d="M7.8 2h8.4C19.4 2 22 4.6 22 7.8v8.4a5.8 5.8 0 0 1-5.8 5.8H7.8C4.6 22 2 19.4 2 16.2V7.8A5.8 5.8 0 0 1 7.8 2m-.2 2A3.6 3.6 0 0 0 4 7.6v8.8C4 18.39 5.61 20 7.6 20h8.8a3.6 3.6 0 0 0 3.6-3.6V7.6C20 5.61 18.39 4 16.4 4H7.6m9.65 1.5a1.25 1.25 0 0 1 1.25 1.25A1.25 1.25 0 0 1 17.25 8 1.25 1.25 0 0 1 16 6.75a1.25 1.25 0 0 1 1.25-1.25M12 7a5 5 0 0 1 5 5 5 5 0 0 1-5 5 5 5 0 0 1-5-5 5 5 0 0 1 5-5m0 2a3 3 0 0 0-3 3 3 3 0 0 0 3 3 3 3 0 0 0 3-3 3 3 0 0 0-3-3z"></path>
                </svg></a>
                <?php endif; ?>
                <?php if($v['or_link']): ?>
                <div class="shareIcon">
                <a href="<?=$v['or_link']?>" target="_blank" style="width: 1em;">
                    <img src="<?=base_url();?>assets/images/openrice_icon.f712ccf7.png" alt="#openrice" class="orIcon">
                </a>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<div class="promote_layer">
    <div class="promote_title">店家優惠<div class="jss818" unselectable="on">&nbsp;</div></div>
    <div style="display:grid;">
        <div class="promote_container">
            <?php foreach($promotions as $pk => $pv): ?>
            <?php echo form_open('editPost'); ?>
            <div class="promote_card" id="post_<?=$pk?>">
                <div class="promote_image" style="background-image: url(<?=$pv['image']?>);"></div>
                <div class="promote_image_stored" style="display:none"><?=$pv['image']?></div>
                <div class="promote_listing_content">
                    <p class="promotion_shopname"><?=$v['name']?></p>
                    <div class="promotion_title"><?=$pv['title']?></div>
                    <div class="promotion_desc"><?=$pv['desc']?>【DSE 學生留意：滿腹食堂】【DSE 學生留意：滿腹食堂】</div>
                    <p class="promotion_expiry" value="<?=date("Y-m-d\TH:i",strtotime($pv['expired_at']))?>">有效期至<?=date("Y年m月d日 H:i",strtotime($pv['expired_at']))?></p>
                    <div class="promotion_tag" value="<?=implode(',',$pv['hashtag'])?>">
                        <?php foreach($pv['hashtag'] as $tag): ?>
                        <span>#<?=$tag?></span>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div>
                    <div class="edit_promotion_btn_container">
                        <button type="button" class="edit_promotion_btn" onclick="edit('post_<?=$pk?>')">
                            <span class="promotion_btn_label">
                                <!-- <h5 class="promotion_button_shopname"><?=$v['name']?></h5> -->
                                <h5 class="promotion_button_shopname">編輯</h5>
                                <svg class="right_arrow" focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"></path></svg>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            </form>
            <?php endforeach; ?>
        </div> 
    </div>
</div>
<?php endforeach;?>
<div id="" class="promotion_edit_modal" style="display:none;">
    <div class="promotion_edit_modal_content">
        <div class="promotion_edit_edit_container">
            <?php echo form_open('home'); ?>
                <div class="promotion_edit_image">
                    <div class="promote_image_hover"></div>
                </div>
                <div class="promotion_edit_content">
                <input type="file" id="imgupload" name="imgupload" style="display:none"/>
                <input type="hidden" name="promotion_edit_image_stored" value=""></input>
                <h4 class="promotion_edit_shopname"></h4>
                <table class="edit_content" style="width:100%;">
                    <tr>
                        <td>Title: </td>
                        <td><input type="text" class="promotion_edit_title" name="promotion_title" value=""></input></td>
                    </tr>
                    <tr>
                        <td>Caption: </td>
                        <td><textarea type="text" class="promotion_edit_caption" name="promotion_edit_caption"></textarea></td>
                    </tr>
                    <tr>
                        <td>Description: </td>
                        <td><textarea type="text" class="promotion_edit_desc" name="promotion_edit_desc"></textarea></td>
                    </tr>
                    <tr>
                        <td>Target: </td>
                        <td>
                            <?php foreach($defaults['target'] as $targets => $target): ?>
                                <div class="promotion_edit_mc">
                                    <input type="radio" id="target<?=$target['id'];?>" class="promotion_edit_target" name="promotion_edit_target[]" value="<?=$target['desc'];?>"></input>
                                    <label for="target<?=$target['id'];?>"><?=$target['desc'];?></label>
                                </div>
                            <?php endforeach; ?>
                            <div class="promotion_edit_mc_other">
                                <input type="radio" id="target_other" class="promotion_edit_target" name="promotion_edit_target[]" value="其他"></input>
                                <label for="target_other">其他:</label>
                                <input type="text" class="promotion_edit_target_other" name="promotion_edit_target_other"></input>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Type: </td>
                        <td>
                            <?php foreach($defaults['type'] as $types => $type): ?>
                                <div class="promotion_edit_mc">
                                    <input type="radio" id="type<?=$type['id'];?>" class="promotion_edit_type" name="promotion_edit_type[]" value="<?=$type['desc'];?>"></input>
                                    <label for="type<?=$type['id'];?>"><?=$type['desc'];?></label>
                                </div>
                            <?php endforeach; ?>
                            <div class="promotion_edit_mc_other">
                                <input type="radio" id="type_other" class="promotion_edit_type" name="promotion_edit_type[]" value="其他"></input>
                                <label for="type_other">其他:</label>
                                <input type="text" class="promotion_edit_type_other" name="promotion_edit_type_other"></input>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Item: </td>
                        <td><input type="text" class="promotion_edit_contraint" name="promotion_edit_constraint"></input></td>
                    </tr>
                    <tr>
                        <td>Constraint: </td>
                        <td><input type="text" class="promotion_edit_contraint" name="promotion_edit_constraint"></input></td>
                    </tr>
                    <tr>
                        <td>Upfront Expense: </td>
                        <td><input type="text" class="promotion_edit_uexpense" name="promotion_edit_uexpense"></input></td>
                    </tr>
                    <tr>
                        <td>Tags: <div class="add_tags">+</div></td>
                        <td>
                            <div class="promotion_edit_tag"> 1.)
                                <input id="promotion_edit_tag" class="promotion_tag_textarea" name="promotion_tag[]"></textarea>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>刊登日期: </td>
                        <td>
                        <input type="datetime-local" value="<?=date("Y-m-d\TH:i",strtotime($pv['expired_at']))?>">
                        <div> 至 </div>
                        <input type="datetime-local" value="<?=date("Y-m-d\TH:i",strtotime($pv['expired_at']))?>">
                        </td>
                    </tr>
                </table>
                </div>
                <button type="button" class="edit_cancel" onclick="$('.promotion_edit_modal').css('display','none');">
                    <span class="edit_cancel_span">取消</span>
                </button>
                <button type="button" class="edit_submit" onclick="preview();">
                    <span class="edit_submit_span">預覽</span>
                </button>
            </form>
        </div>
    </div>
</div>
<div id="" class="promotion_preview_modal">
    <div class="promotion_preview_modal_content">
        <div class="promotion_preview_modal_container">
            <!-- <span onclick="document.getElementById('preview_post_<?=$pk?>').style.display='none'" class="w3-button w3-display-topright">取消&times;</span> -->
            <!-- <?php echo form_open('editPost'); ?> -->
                <div class="promotion_preview_image">
                    <!-- <div class="promote_image_hover"></div> -->
                </div>
                <input type="file" id="imgupload" name="imgupload" style="display:none"/>
                <input type="hidden" name="promotion_preview_image_stored" value=""></input>
                <h4 class="promotion_preview_shopname"></h4>
                <div class="promotion_preview_title" name="promotion_title"></div>
                <div class="promotion_preview_desc" id="promotion_preview_desc" name="promotion_desc"></div>
                <div class="promotion_preview_tag">
                    <div id="promotion_tag_textarea" class="promotion_tag_textarea" name="promotion_tag[]"></div>
                </div>
                <button type="button" class="preview_cancel" onclick="$('.promotion_preview_modal').css('display','none');$('.promotion_edit_modal').css('display','block');">
                    <span class="preview_cancel_span">返回</span>
                </button>
                <button type="button" class="preview_submit">
                    <span class="preview_submit_span">保存變更</span>
                </button>
            <!-- </form> -->
        </div>
    </div>
</div>
<script>
    function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                                    
                reader.onload = function(e) {
                    $('.promotion_edit_image').css('background-image', 'url('+e.target.result+')');
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }
    $("#imgupload").change(function() {readURL(this);});
    $(".promotion_edit_image").click(function () {$("#imgupload").click();});
</script>
<script>
    $(document).ready(function(){
        // window.history.replaceState('', '', '/shopregister/home');
    });

    var textarea = document.getElementById("promotion_preview_desc");
    textarea.oninput = function() {
        textarea.style.height = "";
        textarea.style.height = Math.min(textarea.scrollHeight) + "px";
    };
    textarea.onclick = function() {
        textarea.style.height = "";
        textarea.style.height = Math.min(textarea.scrollHeight) + "px";
    };
    textarea.onblur = function() {
        textarea.style.height = "22px";
    };

    function preview(){
        // var $preview_box = $('#preview_'+id);
        var $edit_box = $('.promotion_edit_modal');
        $edit_box.css('display','none');
        var $preview_box = $('.promotion_preview_modal');
        $preview_box.css('display','block');
        var currentImage = $edit_box.find('.promotion_edit_image').css('background-image');
        var imageSrc = currentImage.match(/url\(\"(.*)\"\)/);
        var shopname = $edit_box.find('.promotion_edit_shopname').text(); 
        var summary = $edit_box.find('.promotion_edit_desc').text();
        var title = $edit_box.find('.promotion_edit_title').text();
        var imageStored = $edit_box.find('.promote_edit_image_stored').text();
        var expired_at = $edit_box.find('.promotion_edit_expiry').val();
        var tags = $edit_box.find('.promotion_edit_tag').attr('value');
        $preview_box.find('.promotion_preview_image').css('background-image','url('+imageSrc[1]+')');
        $preview_box.find('.promotion_preview_shopname').html(shopname);
        $preview_box.find('.promotion_preview_title').html(title);
        $preview_box.find('.promotion_preview_desc"]').html(summary); 
        // $preview_box.find('').html(summary);
        $preview_box.find('input[name="promotion_preview_image_stored"]').val(imageStored); 
        $preview_box.find('#promotion_tag_textarea').text(tags);
    }

    function edit(id){
        // var $edit_box = $('#edit_'+id);
        var $edit_box = $('.promotion_edit_modal');
        $edit_box.attr('id','edit_'+id);
        $edit_box.css('display','block');
        var currentImage = $('#'+id+' > .promote_image').css('background-image');
        var imageSrc = currentImage.match(/url\(\"(.*)\"\)/);
        var shopname = $('#'+id+' .promotion_shopname').text();
        var summary = $('#'+id+' .promotion_desc').text();
        var title = $('#'+id+' .promotion_title').text();
        var imageStored = $('#'+id+ ' .promote_image_stored').text();
        var expired_at = $('#'+id+' .promotion_expiry').val();
        var tags = $('#'+id+' .promotion_tag').attr('value');
        $edit_box.find('.promotion_edit_image').css('background-image','url('+imageSrc[1]+')');
        $edit_box.find('.promotion_edit_shopname').text(shopname);
        $edit_box.find('.promotion_edit_title').attr('value',title);
        $edit_box.find('.promotion_edit_desc').text(summary); 
        // $edit_box.find('').html(summary);
        $edit_box.find('input[name="promotion_edit_image_stored"]').val(imageStored); 
        $edit_box.find('#promotion_tag_textarea').text(tags);
    }

    $('.add_tags').on('click',function(){
        var tag_counts = document.getElementsByClassName("classname").length;
        var tag_input = document.createElement('div');
        tag_input.className = 'promotion_edit_tag';
        tag_input.innerHTML = ''
        $('.promotion_edit_tag').parent('th').append()
    })
</script>
