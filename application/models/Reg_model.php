<?php
    class Reg_model extends CI_Model{
        public function __construct(){
            $this->load->database();
        }

        /** insert data into table for registration */
        public function create_user($data = ''){
            $busReg = $data->BRfilename;
            $busIcon = $data->BIfilename;

            /** insert record into table user */
            $data_user = array(
                'email' => $this->input->post('email'),
                'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
            );
            $this->insert_user($data_user);

            /** obatin latlng through online api */
            $address = $this->input->post('address');
            $latlng = $this->translate_addressToLatlng($address);

            /** insert record into table shop */
            $data_shop = array(
                'name' => $this->input->post('name'),
                'desc' => $this->input->post('desc'),
                'address' => $address,
                'tel_no' => $this->input->post('telno'),
                'industry_id' => $this->input->post('industry'),
                'businessReg' => $busReg,
                'fb_link' => $this->input->post('fb_link'),
                'ig_link' => $this->input->post('ig_link'),
                'or_link' => $this->input->post('or_link'),
                'lat' => $latlng['wgsLat'],
                'lng' => $latlng['wgsLong']
            );
            $this->insert_shop($data_shop);

            /** insert record into table 'user_shop' */
            $this->insert_user_shop_map($this->input->post('email'), $busReg);

            /** insert record into table 'image' */
            $data_image = array(
                'type_id' => 1,
                'name' => $this->input->post('name').' icon',
                'uri' => 'uploads/businessIcon/'.$busIcon
            );
            $this->insert_image($data_image);

            /** insert record into table 'user_image_map' */
            $this->insert_shop_image_map($busIcon, $busReg);
            return true;
        }

        public function insert_user($data_user){
            return $this->db->insert('user',$data_user);
        }

        public function insert_shop($data_shop){
            return $this->db->insert('shop',$data_shop);
        }

        public function insert_user_shop_map($email, $busReg){
            $this->db->select('id');
            $query_user = $this->db->get_where('user', array('email' => $email));
            $get_user_id = $query_user->row_array();

            $this->db->select('id');
            $query_shop = $this->db->get_where('shop', array('businessReg' => $busReg));
            $get_shop_id = $query_shop->row_array();

            $user_shop_map = array(
                'user_id' => $get_user_id['id'],
                'shop_id' => $get_shop_id['id']
            );
            return $this->db->insert('user_shop_map',$user_shop_map);
        }

        public function insert_image($data_image){
            return $this->db->insert('image',$data_image);
        }

        public function insert_shop_image_map($busIcon, $busReg){
            $this->db->select('id');
            $query_image = $this->db->get_where('image', array('uri' => 'uploads/businessIcon/'.$busIcon));
            $get_image_id = $query_image->row_array();

            $this->db->select('id');
            $query_shop = $this->db->get_where('shop', array('businessReg' => $busReg));
            $get_shop_id = $query_shop->row_array();

            $data_shop_image = array(
                'shop_id' => $get_shop_id['id'],
                'image_id' => $get_image_id['id']
            );
            return $this->db->insert('shop_image_map',$data_shop_image);
        }

        public function translate_addressToLatlng($address){
            $atohk1980json = file_get_contents('https://geodata.gov.hk/gs/api/v1.0.0/locationSearch?q='.urlencode(strval($address)));
            $hk1980 = json_decode($atohk1980json, true);
            $hk1980tolatlngjson = file_get_contents('http://www.geodetic.gov.hk/transform/v2/?inSys=hkgrid&e='.urlencode(strval($hk1980[0]['x'].'')).'&n='.urlencode(strval($hk1980[0]['y'].'')));
            $latlng = json_decode($hk1980tolatlngjson, true);
            /** insert into table 'shop' */
            if(isset($latlng['ErrorCode'])) $latlng = array('wgsLat' => 0, 'wgsLong' => 0);
            return $latlng;
        }


        /** get row from table for comparing login/registration details*/
        public function get_reg_data(){
            $email = $this->input->post('email');
            $busReg = $this->input->post('businessReg');

            $this->db->select('email, password, is_verified');
            $query_user = $this->db->get_where('user', array('email' => $this->db->escape_str($email)));
            $get_user = $query_user->row_array();

            $this->db->select('id, businessReg');
            $query_shop = $this->db->get_where('shop', array('businessReg' => $this->db->escape_str($busReg)));
            $get_shop = $query_shop->row_array();

            if($get_shop['id']){
                $this->db->select('is_verified');
                $query_shop = $this->db->get_where('user_shop_map', array('shop_id' => $get_shop['id']));
                $get_verified = $query_shop->row_array();
            } else $get_verified['is_verified'] = 0;

            $sql = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'wnt' AND TABLE_NAME   = 'shop'";
            $shop_ai_number = $this->db->query($sql);
            $get_shop_ai = $shop_ai_number->row_array();

            $sql = "SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'wnt' AND TABLE_NAME   = 'image'";
            $image_ai_number = $this->db->query($sql);
            $get_image_ai = $image_ai_number->row_array();

            $data = array(
                'email' => $get_user['email'],
                'password' => $get_user['password'],
                'user_flag' => $get_user['is_verified'],
                'businessReg' => $get_shop['businessReg'],
                'shop_flag' => $get_verified['is_verified'],
                'shop_ai' => $get_shop_ai['AUTO_INCREMENT'],
                'image_ai' => $get_image_ai['AUTO_INCREMENT']
            );
            return $data;
        }

        public function get_user_id($email){
            $this->db->select('id');
            $query_user = $this->db->get_where('user', array('email' => $this->db->escape_str($email)));
            return $query_user->row_array();
        }

        public function get_shop_details($user_id = 0){
            $sql = "SELECT s.id, s.name, s.desc, s.address, s.tel_no, siy.desc as industry, s.fb_link, s.ig_link, s.or_link, s.lat, s.lng
                    FROM user u
                    LEFT JOIN user_shop_map us ON u.id = us.user_id
                    LEFT JOIN shop s ON s.id = us.shop_id
                    LEFT JOIN shop_industry siy ON siy.id = s.industry_id
                    WHERE u.id = $user_id";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            return $data;
        }

        public function get_user_icon($shop_id = 0){
            $sql = "SELECT i.uri
                    FROM shop s
                    LEFT JOIN shop_image_map si ON s.id = si.shop_id
                    LEFT JOIN image i ON i.id = si.image_id
                    WHERE s.id = $shop_id AND i.type_id = 1
                    ORDER BY i.id DESC";
            $query = $this->db->query($sql);
            $data = $query->row_array();
            return $data['uri'];
        }

        public function get_user_details($user_id = 0){
            $this->db->select('id, email, password, is_verified');
            $query_user = $this->db->get_where('user', array('id' => $user_id));
            return $query_user->row_array();
        }

        public function update_user_password($user_id, $newpassword){
            $sql = "UPDATE user
                    SET password = '$newpassword'
                    WHERE id = $user_id";
            $query = $this->db->query($sql);
            if ($this->db->affected_rows() > 0) return true;
            else return false;
        }

        // public function create_promotion($data = array()){
        //     /** insert record into table user */
        //     $data_promotion = array(
        //         'title' => $this->input->post('promotion_title'),
        //         // 'shop_id' => 
        //     );
        //     $this->insert_promotion($data_promotion);
        // }

        // public function insert_promotion_hashtag($data_promotion_hashtag){
        //     return $this->db->insert('promotion_hashtag_map',$data_promotion_hashtag);
        // }

        public function get_promotion($shop_id = 0){
            $query_promotion = $this->db->get_where('promotion', array('shop_id' => $shop_id));
            $get_promotion = $query_promotion->result_array();
            foreach($get_promotion as $k => $v){
                $sql = "SELECT h.desc
                        FROM hashtag h 
                        LEFT JOIN promotion_hashtag_map ph ON ph.hashtag_id = h.id
                        LEFT JOIN promotion p ON p.id = ph.promotion_id
                        WHERE p.shop_id = ".$shop_id."
                        ORDER BY h.id DESC
                        LIMIT 0,3";
                $query = $this->db->query($sql);
                $hashtag = $query->result_array();
                $get_promotion[$k]['hashtag'] = $hashtag[0];
                $query_image = $this->db->get_where('image', array('id' => $v['image_id']));
                $get_image = $query_image->row_array();
                $get_promotion[$k]['image'] = $get_image['uri'];
            }
            // echo "<pre>";
            // print_r($get_promotion);
            // echo "</pre>";
            return $get_promotion;
        }

        public function get_defaults(){
            $query_promotion_type = $this->db->get('promotion_type');
            $get_promotion_type = $query_promotion_type->result_array();
            $query_promotion_target = $this->db->get('promotion_target');
            $get_promotion_target = $query_promotion_target->result_array();
            $defaults['type'] = $get_promotion_type;
            $defaults['target'] = $get_promotion_target;
            return $defaults;
        }

        public function add_promotion($shop_id = 0, $data = array()){
            $data_promotion = array(
                'shop_id' => $data['shop_id']
            );
        }

        // public function update_promotion($shop_id = 0, $data = array()){

        // }
    }
