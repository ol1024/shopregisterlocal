<?php
    class Registration_controller extends CI_Controller{

        protected $BRfilename;
        protected $BIfilename;

        public function __construct(){
            parent::__construct();
            if($this->session->userdata('logged_in')==true){
                redirect('welcome/index');
            }
        }

        /** index page */
        public function index($data = array()){
            $data = array_merge(array(
                'email' => '',
                'password' => '',
                'passwordConfirm' => '',
                'industry' => '',
                'name' => '',
                'businessReg'=> '',
                'address' => '',
                'telno' => '',
                'businessIcon'=> '',
                'desc'=> '',
                'fb_link' => '',
                'ig_link' => '',
                'or_link' => '',
                'error' => ''
            ), $data);
			$this->load->view('partial/header');
			$this->load->view('partial/navbar');
            $this->load->view('user/register', $data);
            $this->load->view('partial/footer');
        }

        public function validate(){
            global $BRfilename;
            global $BIfilename;
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
                'passwordConfirm' => $this->input->post('passwordConfirm'),
                'industry' => $this->input->post('industry'),
                'name' => $this->input->post('name'),
                'businessReg'=>$_FILES['businessReg'],
                'address' => $this->input->post('address'),
                'telno' => $this->input->post('telno'),
                'businessIcon'=>$_FILES['businessIcon'],
                'desc'=>$this->input->post('desc'),
                'fb_link' => $this->input->post('fb_link'),
                'ig_link' => $this->input->post('ig_link'),
                'or_link' => $this->input->post('or_link'),
            );
            $this->load->helper('file');
            $this->load->library('upload');
            $this->form_validation->set_rules('email', '電郵地址', 'required|callback_validate_email');
            $this->form_validation->set_rules('password', '密碼', 'required');
            $this->form_validation->set_rules('passwordConfirm', '確認密碼', 'required|callback_validate_passwordConfirm');
            $this->form_validation->set_rules('industry', '商店種類', 'required');
            $this->form_validation->set_rules('name', '商店名稱', 'required');
            $this->form_validation->set_rules('address', '商戶地址', 'required');
            $this->form_validation->set_rules('telno', '聯絡電話', 'required|callback_validate_telno');
            $this->form_validation->set_rules('businessReg', '商業登記證', 'callback_validate_businessReg');
            $this->form_validation->set_rules('businessIcon', '商戶大頭照', 'callback_validate_businessIcon');

            $this->form_validation->set_message('required','請輸入{field}');

            if($this->form_validation->run() === FALSE){
                $data['error'] = 1;
                $this->index($data);
            } else {
                $valid = new stdClass();
                $valid->BRfilename = $BRfilename;
                $valid->BIfilename = $BIfilename;
                if($this->reg_model->create_user($valid)){
                    $this->success(1);
                } else {
                    redirect('welcome');
                }
            }
        }

        public function validate_businessReg(){
            if(empty($_FILES['businessReg']['name'])){
                $this->form_validation->set_message('validate_businessReg','請跟據指示上傳商業登記證副本');
                return false;
            }
            if($this->upload_businessReg()) return true;
            else return false;
        }

        public function validate_businessIcon(){
            if(empty($_FILES['businessIcon']['name'])){
                $this->form_validation->set_message('validate_businessIcon','請跟據指示上傳商業登記證副本');
                return false;
            }

            if($this->upload_businessIcon()) return true;
            else return false;
        }

        public function validate_email(){
            if(!filter_var($this->input->post('email'), FILTER_VALIDATE_EMAIL)){
                $this->form_validation->set_message('validate_email','電郵地址格式不正確，請重新輸入');
                return false;
            }
            
            //compare input email with database
            $get_data = $this->reg_model->get_reg_data();
            if($this->input->post('email') == $get_data['email']){
                $this->form_validation->set_message('validate_email','電郵地址已被登記，請重新輸入電郵地址');
                return false;
            }
            else return true;
        }

        public function validate_passwordConfirm(){
            if($this->input->post('password') != $this->input->post('passwordConfirm')){
                $this->form_validation->set_message('validate_passwordConfirm','密碼與確認密碼不相符，請重新輸入');
                return false;
            } else return true;
        }

        public function validate_telno(){
            if(!preg_match("/^[0-9]{8}$/",$this->input->post('telno'))){
                $this->form_validation->set_message('validate_telno','聯絡電話須為8位數字，請重新輸入');
                return false;
            } else return true;
        }

        public function upload_businessReg(){
            global $BRfilename;
            $get_data = $this->reg_model->get_reg_data();
            $config['upload_path'] = 'uploads/businessReg/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size'] = '2000'; // max_size in kb
            $config['file_name'] =  'i_'.$get_data['shop_ai'].'_br';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);

            if($this->upload->do_upload('businessReg')){
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $BRfilename = 'i_'.$get_data['shop_ai'].'_br';
                return true;
            }else{
                $error = $this->upload->show_errors();
                if(preg_match('/The uploaded file exceeds the maximum allowed size in your PHP configuration file/', $error[0])){
                    $this->form_validation->set_message('validate_businessReg','上傳文件(商業登記證)超出2MB上限，請重新上傳');
                }elseif(preg_match('/The filetype you are attempting to upload is not allowed/', $error[0])){
                    $this->form_validation->set_message('validate_businessReg','上傳格式(商業登記證)只限於jpg/jpeg/png/pdf，請重新上傳');   
                }else{
                    $this->form_validation->set_message('validate_businessReg',$error[0]);
                }
                return false;
            }
        }

        public function upload_businessIcon(){
            global $BIfilename;
            $get_data = $this->reg_model->get_reg_data();
            $config2['upload_path'] = 'uploads/businessIcon/';
            $config2['allowed_types'] = 'jpg|jpeg|png';
            $config2['max_size'] = '2000'; // max_size in kb
            $config2['file_name'] = 'i_'.$get_data['shop_ai'].'_i_'.$get_data['image_ai'];
            $config2['overwrite'] = TRUE;
            $this->upload->initialize($config2);

            if($this->upload->do_upload('businessIcon')){
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $BIfilename = 'i_'.$get_data['shop_ai'].'_i_'.$get_data['image_ai'];
                return true;
            }else{
                $error = $this->upload->show_errors();
                if(preg_match('/The uploaded file exceeds the maximum allowed size in your PHP configuration file/', $error[0])){
                    $this->form_validation->set_message('validate_businessIcon','上傳文件(商戶大頭照)超出2MB上限，請重新上傳');
                }
                elseif(preg_match('/The filetype you are attempting to upload is not allowed/', $error[0])){
                    $this->form_validation->set_message('validate_businessIcon','上傳格式(商戶大頭照)只限於jpg/jpeg/png，請重新上傳');
                }
                return false;
            }
        }

        /** display the success page after verified */
        public function success($success){
            if($this->input->post('success') == "1" || $success == 1){
                $data['title'] = '登記成功';
				$this->load->view('partial/header');
				$this->load->view('partial/navbar');
                $this->load->view('user/success', $data);
                $this->load->view('partial/footer');
            } else {
                redirect('registration');
            }
        }
    }
