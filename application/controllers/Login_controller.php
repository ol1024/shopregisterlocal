<?php
    class Login_controller extends CI_Controller{
        /** check if user is logined, redirect to welcome page if so */
        public function __construct(){
            parent::__construct();
            if($this->session->userdata('logged_in')){
                redirect('home');
            }
        }

        /** verify input after user click submit */
        public function index(){
            $data = array(
                'email' => $this->input->post('email'),
                'password' => $this->input->post('password'),
            );
            $this->form_validation->set_rules('email','Email', 'trim|required|valid_email|callback_validate_email');
            $this->form_validation->set_rules('password','Password', 'required|callback_validate_password|callback_validate_verified');

            $this->form_validation->set_message('required', '請輸入帳戶資料後登入');
            $this->form_validation->set_message('valid_email', '電郵地址格式錯誤，請檢查電郵地址');
            $this->form_validation->set_message('validate_email','帳戶不存在，請檢查電郵地址');
            $this->form_validation->set_message('validate_password','密碼錯誤！請重新輸入');
            $this->form_validation->set_message('validate_verified','抱歉，帳戶尚未審查成功，請耐心等候');

            if($this->form_validation->run() === FALSE){
				$this->load->view('partial/header');
				$this->load->view('partial/navbar');
                $this->load->view('user/login', $data);
                $this->load->view('partial/footer');
            } else {
                $user_id = $this->reg_model->get_user_id($data['email']);
                $session_data = array(
                    'user_id'   => $user_id['id'],
                    'email'     => $data['email'],
                    'logged_in' => TRUE,
                );
                $this->session->set_userdata($session_data);
                redirect('home');
            }
        }

        /** form_validation rules to validate if email is registered */
        public function validate_email(){
            $email = $this->input->post('email');
            $get_data = $this->reg_model->get_reg_data();
            if($email == $get_data['email']) return true;
            else return false;
        }

        /** form_validation rules to validate if password matches */
        public function validate_password(){
            $password = $this->input->post('password');
            $get_data = $this->reg_model->get_reg_data();
            if(password_verify($password, $get_data['password'])) return true;
            else return false;
        }

        /** form_validation rules to validate if user is verified */
        public function validate_verified(){
            $get_data = $this->reg_model->get_reg_data();
            if($get_data['user_flag'] == 1) return true;
            else return false;
		}
    }
