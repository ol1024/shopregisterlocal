<?php
    class Home_controller extends CI_Controller{

        protected $user_id;
        
        /** check if user is logined, redirect to welcome page if so */
        public function __construct(){
            global $user_id;
            parent::__construct();
            if($this->session->userdata('logged_in')!=true){
                redirect('/');
            }
            else{
                $user_id = $this->session->userdata('user_id');
            }
        }

        /** index page */
        public function index($msg = null){
            global $user_id;
            $get_data = $this->reg_model->get_shop_details($user_id);
            $data = array();
            foreach($get_data as $g => $d){
                $get_icon = $this->reg_model->get_user_icon($d['id']);
                $data[$g] = array(
                    "name" => $d['name'],
                    "desc" => $d['desc'],
                    "address" => $d['address'],
                    "tel_no" => $d['tel_no'],
                    "industry" => $d['industry'],
                    "fb_link" => $d['fb_link'],
                    "ig_link" => $d['ig_link'],
                    "or_link" => $d['or_link'],
                    "lat" => $d['lat'],
                    "lng" => $d['lng'],
                    "icon" => $get_icon
                );
                $promotions = $this->reg_model->get_promotion($d['id']);
            }
            $defaults = $this->reg_model->get_defaults();
			$this->load->view('partial/header');
			$this->load->view('partial/navbar');
            $this->load->view('user/home', array('data'=>$data, 'msg'=>$msg, 'promotions'=>$promotions, 'defaults'=>$defaults));
            $this->load->view('partial/footer');
        }

        public function edit(){
            global $user_id;
            $get_data = $this->reg_model->get_shop_details($user_id);
            foreach($get_data as $g => $d){
                $get_icon = $this->reg_model->get_user_icon($d['id']);
                $data[$g] = array(
                    "name" => $d['name'],
                    "desc" => $d['desc'],
                    "address" => $d['address'],
                    "tel_no" => $d['tel_no'],
                    "industry" => $d['industry'],
                    "fb_link" => $d['fb_link'],
                    "ig_link" => $d['ig_link'],
                    "or_link" => $d['or_link'],
                    "lat" => $d['lat'],
                    "lng" => $d['lng'],
                    "icon" => $get_icon
                );
            }
			$this->load->view('partial/header');
			$this->load->view('partial/navbar');
            $this->load->view('user/editPassword', $data);
            $this->load->view('partial/footer');
        }

        public function editPw(){
            $data = array(
                'oldpassword' => $this->input->post('oldpassword'),
                'newpassword' => $this->input->post('newpassword'),
                'newpasswordConfirm' => $this->input->post('newpasswordConfirm')
            );
            $this->form_validation->set_rules('oldpassword','現正使用密碼', 'required|callback_validate_password');
            $this->form_validation->set_rules('newpassword','新密碼', 'required');
            $this->form_validation->set_rules('newpasswordConfirm','確認新密碼', 'required|callback_validate_newpassword|callback_validate_newpasswordConfirm');

            $this->form_validation->set_message('required', '這為必需填寫的欄位，請輸入{field}');
            $this->form_validation->set_message('validate_password','現正使用密碼輸入錯誤！請重新輸入');
            $this->form_validation->set_message('validate_newpassword','新密碼與現正使用密碼相同，請重新輸入');
            $this->form_validation->set_message('validate_newpasswordConfirm','新密碼與確認新密碼不相同，請重新輸入');

            if($this->form_validation->run() === FALSE){
				$this->load->view('partial/header');
				$this->load->view('partial/navbar');
                $this->load->view('user/editPassword', $data);
                $this->load->view('partial/footer');
            } else {
                $this->updatePassword();
                $this->index('密碼已更改');
            }
        }

        public function validate_password(){
            global $user_id;
            $password = $this->input->post('oldpassword');
            $get_data = $this->reg_model->get_user_details($user_id);
            if(password_verify($password, $get_data['password'])) return true;
            else return false;
        }

        public function validate_newpassword(){
            global $user_id;
            $newpassword = $this->input->post('newpassword');
            $get_data = $this->reg_model->get_user_details($user_id);
            if(password_verify($newpassword, $get_data['password'])) return false;
            else return true;
        }

        public function validate_newpasswordConfirm(){
            $newpassword = $this->input->post('newpassword');
            $newpasswordConfirm = $this->input->post('newpasswordConfirm');
            if($newpassword == $newpasswordConfirm) return true;
            else return false;
        }

        public function updatePassword(){
            global $user_id;
            $newpassword = $this->input->post('newpassword');
            $hash = password_hash($newpassword, PASSWORD_DEFAULT);
            $this->reg_model->update_user_password($user_id, $hash);
        }

        public function logout(){
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('logged_in');
            $this->session->sess_destroy();
            redirect('/');
        }

        public function editPost(){
            global $imagefilename;
            $data_image = array(
                'imgupload' => $this->input->post('imgupload'),
                'promote_image_stored' => $this->input->post('promote_image_stored'),
            );
            $data = array(
                'shop_id' => $this->input->post('shop_id'),
                'title' => $this->input->post('promotion_title'),
                'caption' => $this->input->post('promotion_caption'),
                'desc' => $this->input->post('promotion_desc'),
                'item' => $this->input->post('item'),
                'constraint' => $this->input->post('constraint'),
                'upfront_expense'=>$_FILES['upfront_expense'],
                'value' => $this->input->post('value'),
                'telno' => $this->input->post('telno'),
                'rank' => $this->input->post('rank'),
                'valid_date' => $this->input->post('valid_date'),
                'is_active' => $this->input->post('is_active'),
                'expired_at' => $this->input->post('expired_at'),
                'created_at' => $this->input->post('created_at'),
            ); 
            

            $this->
            $this->index();
        }

        public function addPost(){
            $data = array(
                'shop_id' => $this->input->post('shop_id'),
                'title' => $this->input->post('promotion_title'),
                'caption' => $this->input->post('promotion_caption'),
                'desc' => $this->input->post('promotion_desc'),
                'item' => $this->input->post('item'),
                'constraint' => $this->input->post('constraint'),
                'upfront_expense'=>$_FILES['upfront_expense'],
                'value' => $this->input->post('value'),
                'telno' => $this->input->post('telno'),
                'rank' => $this->input->post('rank'),
                'valid_date' => $this->input->post('valid_date'),
                'is_active' => $this->input->post('is_active'),
                'expired_at' => $this->input->post('expired_at'),
                'created_at' => $this->input->post('created_at'),
            );

        }

        public function upload_image(){
            global $BIfilename;
            $get_data = $this->reg_model->get_reg_data();
            $config2['upload_path'] = 'uploads/businessIcon/';
            $config2['allowed_types'] = 'jpg|jpeg|png';
            $config2['max_size'] = '2000'; // max_size in kb
            $config2['file_name'] = 'i_'.$get_data['shop_ai'].'_i_'.$get_data['image_ai'];
            $config2['overwrite'] = TRUE;
            $this->upload->initialize($config2);

            if($this->upload->do_upload('businessIcon')){
                // Get data about the file
                $uploadData = $this->upload->data();
                $filename = $uploadData['file_name'];
                $BIfilename = 'i_'.$get_data['shop_ai'].'_i_'.$get_data['image_ai'];
                return true;
            }else{
                $error = $this->upload->show_errors();
                if(preg_match('/The uploaded file exceeds the maximum allowed size in your PHP configuration file/', $error[0])){
                    $this->form_validation->set_message('validate_businessIcon','上傳文件(商戶大頭照)超出2MB上限，請重新上傳');
                }
                elseif(preg_match('/The filetype you are attempting to upload is not allowed/', $error[0])){
                    $this->form_validation->set_message('validate_businessIcon','上傳格式(商戶大頭照)只限於jpg/jpeg/png，請重新上傳');
                }
                return false;
            }
        }

        public function updatePost(){
            global $user_id;
            $newpassword = $this->input->post('newpassword');
            $hash = password_hash($newpassword, PASSWORD_DEFAULT);
            $this->reg_model->update_user_password($user_id, $hash);
        }
    }
