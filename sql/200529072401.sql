/*
MySQL Backup
Source Server Version: 5.5.5
Source Database: wnt
Date: 29/5/2020 07:24:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `hashtag`
-- ----------------------------
DROP TABLE IF EXISTS `hashtag`;
CREATE TABLE `hashtag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `image`
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `uri` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `posts`
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `promotion`
-- ----------------------------
DROP TABLE IF EXISTS `promotion`;
CREATE TABLE `promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `caption` varchar(80) DEFAULT NULL,
  `desc` varchar(500) DEFAULT NULL,
  `image_id` int(11) NOT NULL,
  `target_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `item` varchar(100) DEFAULT NULL,
  `constraint` varchar(80) DEFAULT NULL,
  `upfront_expense` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `rank` int(11) NOT NULL,
  `valid_date` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `expired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `promotion_hashtag_map`
-- ----------------------------
DROP TABLE IF EXISTS `promotion_hashtag_map`;
CREATE TABLE `promotion_hashtag_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` int(255) NOT NULL,
  `hashtag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `promotion_target`
-- ----------------------------
DROP TABLE IF EXISTS `promotion_target`;
CREATE TABLE `promotion_target` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `promotion_type`
-- ----------------------------
DROP TABLE IF EXISTS `promotion_type`;
CREATE TABLE `promotion_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `recommendation`
-- ----------------------------
DROP TABLE IF EXISTS `recommendation`;
CREATE TABLE `recommendation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `caption` varchar(80) DEFAULT NULL,
  `desc` varchar(500) DEFAULT NULL,
  `image_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `recommendation_hashtag_map`
-- ----------------------------
DROP TABLE IF EXISTS `recommendation_hashtag_map`;
CREATE TABLE `recommendation_hashtag_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recommendation_id` int(255) NOT NULL,
  `hashtag_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `recommendation_type`
-- ----------------------------
DROP TABLE IF EXISTS `recommendation_type`;
CREATE TABLE `recommendation_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `shop`
-- ----------------------------
DROP TABLE IF EXISTS `shop`;
CREATE TABLE `shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tel_no` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry_id` int(11) NOT NULL,
  `businessReg` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fb_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `ig_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `or_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `shop_image_map`
-- ----------------------------
DROP TABLE IF EXISTS `shop_image_map`;
CREATE TABLE `shop_image_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `shop_industry`
-- ----------------------------
DROP TABLE IF EXISTS `shop_industry`;
CREATE TABLE `shop_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_verified` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `user_shop_map`
-- ----------------------------
DROP TABLE IF EXISTS `user_shop_map`;
CREATE TABLE `user_shop_map` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `is_verified` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `hashtag` VALUES ('1','FreeHK'), ('2','RevolutionNow');
INSERT INTO `image` VALUES ('1','1','test 31 中文測試 icon','uploads/businessIcon/28_2004070654_photo_2020-02-09_20-44-45.jpg','2020-04-08 00:54:31'), ('2','1','test 32 中文中文勹═齨〔『﹁欍匶麡儕肅璛舀卐 icon','uploads/businessIcon/29_2004070720_06P5653D5E451566B82F88px.jpg','2020-04-08 01:20:21'), ('3','1','test 33 商業登記證副本 icon','uploads/businessIcon/30_2004070733_p5mRNjQgQNh4KMwfVW3EnP.jpg','2020-04-08 01:33:17'), ('4','1','test 35 icon','uploads/businessIcon/','2020-04-16 06:31:17'), ('5','1','test 35 icon','uploads/businessIcon/i_32_i_5','2020-04-17 00:19:59'), ('6','1','test 36 icon','uploads/businessIcon/i_33_i_6','2020-04-17 00:23:59'), ('7','1','test 36 icon','uploads/businessIcon/i_34_i_7.jpg','2020-04-17 01:26:28'), ('8','1','test 39 icon','uploads/businessIcon/i_35_i_8.png','2020-05-09 01:48:10');
INSERT INTO `posts` VALUES ('1','Post One','post-one','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris at fringilla elit. Quisque aliquet condimentum porta. Integer sit amet dignissim nisi, eget consectetur lacus. Nunc placerat laoreet odio, vitae ornare sapien lacinia feugiat. Aenean posuere blandit urna, eget convallis lorem placerat nec. Sed convallis ac purus id convallis. Ut venenatis arcu sit amet massa congue posuere. Mauris blandit interdum egestas. Pellentesque a varius neque, eu sodales augue. Duis libero augue, finibus non sodales et, dictum at metus.','2020-03-29 04:18:38'), ('2','Post Two','post-two','Vivamus eu sapien vel orci faucibus viverra vitae nec eros. Aenean dictum elementum tellus, et auctor ligula ullamcorper quis. Morbi bibendum fermentum faucibus. Suspendisse et dictum mauris. Cras porttitor est quis nunc gravida, eget interdum justo pellentesque. Phasellus aliquam augue tincidunt neque dapibus, in varius tellus convallis. Nunc varius porta ultrices. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam tempor enim ut risus rutrum sagittis. Aenean sit amet feugiat ante. Nulla ex ex, tincidunt eu leo sed, fermentum iaculis lacus. Suspendisse quis est rutrum, dignissim tellus eget, consectetur enim. Morbi gravida in lorem ac congue. Vivamus faucibus vestibulum mi a iaculis. Nunc rhoncus lacus mi, eu posuere mauris elementum non. Etiam gravida ex risus, in porta lectus bibendum ut.','2020-03-29 04:18:38'), ('3','Post Three','Post-Three','This is post number three','2020-03-29 05:39:24');
INSERT INTO `promotion` VALUES ('1','35','【DSE 學生留意：滿腹食堂】','【DSE 學生留意：滿腹食堂】','【DSE 學生留意：滿腹食堂】','8','1','1','',NULL,NULL,NULL,'1',NULL,'1','2020-05-11 03:13:50','2020-05-09 18:59:26');
INSERT INTO `promotion_hashtag_map` VALUES ('1','1','1'), ('2','1','2');
INSERT INTO `promotion_type` VALUES ('1','折扣'), ('2','現金卷'), ('3','免費'), ('4','凡... 送...'), ('5','優惠');
INSERT INTO `recommendation_type` VALUES ('1','文宣'), ('2','理念'), ('3','食物'), ('4','環境'), ('5','主打');
INSERT INTO `shop` VALUES ('1','test retail','','駿洋邨駿逸樓','12345678','2','123456','','','','22.4','114.19'), ('2','test retail','','駿洋邨駿逸樓','12345678','2','234567','','','','22.4','114.19'), ('3','test restaurant','','駿洋邨駿逸樓2','12345678','1','345678','','','','22.4','114.19'), ('4','test restaurant','','駿洋邨駿逸樓3','12345678','1','456789','','','','22.4','114.19'), ('5','test restaurant','','駿洋邨駿逸樓5','12345678','1','567890','','','','22.4','114.19'), ('6','test NGO','','金鐘添美道300號','12345678','3','678901','','','','22.2799','114.166'), ('7','test NGO 2','','金鐘添美道301號','12345678','3','789012','','','','22.2806','114.167'), ('8','test NGO 8','','金鐘添美道8號','09876543','3','890123','','','','22.2799','114.166'), ('9','test 9','','金鐘添美道10號','12345678','3','1234567','','','','22.2799','114.166'), ('10','test 11','','金鐘添美道11號','12345678','3','3456789','','','','22.2799','114.166'), ('11','test 12','','金鐘添美道12號','09876543','2','4567890','','','','22.2799','114.166'), ('12','test 13','','金鐘添美道123號','12345783','2','12345678','','','','22.2799','114.166'), ('13','test 14','','123456','12345678','2','123456789','','','','22.324','114.166'), ('14','test 15','','金鐘添美道266號','12345678','2','123465780','','','','22.2799','114.166'), ('15','123456','','金鐘添美道321號','12345678','2','321','','','','22.2799','114.166'), ('16','test 18','','金鐘添美道18號','09876543','3','','','','','22.2799','114.166'), ('17','test 19','','金鐘添美道19號','12345678','2','','','','','22.2799','114.166'), ('18','test 20','','金鐘添美道20號','12345678','2','','','','','22.2799','114.166'), ('19','test 21','','金鐘添美道21號','12345678','2','','','','','22.2799','114.166'), ('20','test 22','','123456','12345678','3','','','','','22.324','114.166'), ('21','test 24','','金鐘添美道24號','12345678','3','','','','','22.2799','114.166'), ('22','test 25','','駿洋邨駿逸樓25','12345678','2','','','','','22.4','114.19'), ('23','test 26','','駿洋邨駿逸樓','13245678','3',NULL,'','','','22.4','114.19'), ('24','test 27','','金鐘添美道2號','12345678','2',NULL,'','','','22.2805','114.165'), ('25','test 28','','金鐘添美道2號','12345678','2',NULL,'','','','22.2805','114.165'), ('26','test 29','','金鐘添美道2號','12345678','2',NULL,'','','','22.2805','114.165'), ('27','test 30','','金鐘添美道2號','12345678','2','27_2004061215_photo_2020-02-13_14-31-08.jpg','','','','22.2805','114.165'), ('28','test 31 中文測試','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam rhoncus pellentesque vulputate. Nulla facilisi. Nam et suscipit dolor. Vivamus nec felis luctus, sodales urna ut, ullamcorper nisl. Nulla mollis semper accumsan. Nam venenatis commodo ante, sed eleifend purus consequat sollicitudin. Integer eros tellus, sodales eu congue nec, porta nec leo. Maecenas elit nisl, pulvinar et ultrices eu, maximus id nulla.','金鐘添美道31號','31313131','1','28_2004070654_photo_2020-02-13_14-31-08.jpg','testing.facebook.com/31','testing.instagram.com/31','testing.openrice.com/31','22.2799','114.166'), ('29','test 32 中文中文勹═齨〔『﹁欍匶麡儕肅璛舀卐','222222222222555555555555朽杲屠朽杲屠杳朽','駿洋邨駿逸樓32','32323232','2','29_2004070720_06P564E905D085F7C57601px.jpg','','','','22.4','114.19'), ('30','test 33 商業登記證副本','The Steam Patch for Resident Evil Resistance is now live! It brings updates and optimizations to matchmaking.\nIf you\'re having issues, please reboot the application to ensure you are running the latest version.','RacoonCity 33 street, 33 avenue','33333333','3','30_2004070733_resident-evil-3-remake-wallpaper.jpg','','https://www.residentevil.com/re3/uk/','https://www.residentevil.com/re3/uk/','22.4339','114.006'), ('31','test 35','','金鐘添美道2號','12345678','3','i_31_br','','','','22.2805','114.165'), ('32','test 35','','金鐘添美道35號','12345678','1','i_32_br','','','','22.2799','114.166'), ('33','test 36','','金鐘添美道36號','12345678','1','i_33_br','','','','22.2799','114.166'), ('34','test 36','','金鐘添美道34號','12345678','3','i_34_br','','','','22.2799','114.166'), ('35','test 39','','123456','12345678','2','i_35_br','','','','22.324','114.166');
INSERT INTO `shop_image_map` VALUES ('1','28','1'), ('2','29','2'), ('3','30','3'), ('4','31','4'), ('5','32','5'), ('6','33','6'), ('7','34','7'), ('8','35','8');
INSERT INTO `shop_industry` VALUES ('1','Restaurant'), ('2','Retail'), ('3','NGO');
INSERT INTO `user` VALUES ('1','test@test.test','$2y$10$nBY4gvO2UAsnC8Dq1agWseVlYvMyOpRKtNLLREYqKFCjDeNFJ4/Gq','0'), ('2','test2@test.test','$2y$10$TshAkia5Y4LuipSyuOCLgud.cxA5GvS0Ve3KwY/lwUrUh4iniDSfe','0'), ('3','test3@test.test','$2y$10$/Zpo967wCcqMKr1ncMkKZe0wRpOa1OsslyJeKJPAlkG.4b/.3U4zy','0'), ('4','test4@test.test','$2y$10$zAm0whxeiHRqIb1L60xXQ.UWhsEDla.PgZaNidWyXL2Q1ZAbcjzWS','0'), ('5','test5@test.test','$2y$10$w2fAT./nuL3IJJGoSLSI4eOwqvdQr2mYsrvKui0.i6OMYjY/m9chu','0'), ('6','test6@test.test','$2y$10$aOSHAwH.khIHUh0cV9BCteFWaxNpxOgzk.Q6dU0GWFNCjcyf3ZpSe','0'), ('7','test7@test.test','$2y$10$t8JoiXLRig/Z30eYGfdcNu27EFHhmVKaeyDhaBZF/Lh7p4SCwSHAG','0'), ('8','test8@test.test','$2y$10$Bov7rofp8G5C4SvPeJAhm.1HPLYraM9RvJ6NjBTdNSw6UfaC5npIK','0'), ('9','test9@test.test','$2y$10$nHjbN64u83gA48LPfIHXaeKfPoXdzJmXj3vhBpGONynqVZioJK2t.','0'), ('10','test11@test.test','$2y$10$NZ3pFQ2WX.SO/Ytx0w1FA.G58zFzcz1LJlRMmBl2t4BnkuQNllufS','0'), ('11','test12@test.test','$2y$10$FNUJnvcTZWD9kHCtSTmUyeT2KeCTxDOz5Z.j4i600dEJ4ZskpUKdG','0'), ('12','test13@test.test','$2y$10$i8A7L9it1myn6q8d2PaiMeWJPw/LTOWvQgdNWKbbJ4ePN.VumgAlK','0'), ('13','test14@test.test','$2y$10$hIktci8r68g3b01OqqTEAeYrYH0/I8rF4SO2.rbuh2No1wbEUbBBC','0'), ('14','test15@test.test','$2y$10$Yn59G9tqdOyQcAuC5cpzfuVdvET.1o6H1qXBh.25uozlgcIKf3TKq','0'), ('15','test16@test.test','$2y$10$5u6m/bnINM0jgXQvxmUpGOhVIMyagmZOu8hghT/ZTqEon04lFizVa','1'), ('16','test18@test.test','$2y$10$DBjONzh5G6qHFwfpiZy1..H9yLvBGf5/Bhd969yn16Er6mkalr2BK','0'), ('17','test19@test.test','$2y$10$RjWsMzPXRFx0rZZ/7lQE4ut6eJaP4xQHu15KOoFGT.RqCj1mIB616','0'), ('18','test20@test.test','$2y$10$bem3eSqnjXSR.ZMqJ6CSCO9hO3OPbY1w1tVOVl/M7NXUNkuIIKaZ2','0'), ('19','test21@test.test','$2y$10$dxV8PFZuPOWNkAeilqTkkuzwu.KY35pcNkQz7dLBlgRUn/hR4TlPa','0'), ('20','test22@test.test','$2y$10$40ZoayDMnTZ3.RrD7gIqKeBhgYsANVlHwAzTqlPWCShyCIqCd2sJq','0'), ('21','test24@test.test','$2y$10$Wy2memza4a9c7MDEAB9jNe5poEPzBnds83hXCuyTfJYkJUF9VmChi','0'), ('22','test25@test.test','$2y$10$WhWH9vwPBO6ePXqnqCW3qO.yqhgo7pnFtKY6.Hq3SHhKsKoHbVDAW','0'), ('23','test26@test.test','$2y$10$VG2g3LzEGUfgOeIA/xqAEOKqK6C4Yl9ziPVD1/S90MySms6UkkFee','0'), ('24','test27@test.test','$2y$10$Ypu..1muTLAyJ1ixCsB5WuNg6zQKxxjXrTkEedUO8JDGf0iwB9GOC','0'), ('25','test28@test.test','$2y$10$m9qivddR03na.imjsunyYO0p0zigPlGCNQNDT1ycR1MBnWil6wF16','0'), ('26','test29@test.test','$2y$10$hPxe4TbYbjBtUUQJr0qK9.QyCJcwzhfDmQcUTII4OMfnPzxMSfA9q','0'), ('27','test30@test.test','$2y$10$p42KP2YvPIXF5saFF17hIeE.iagG8wl/qAfV.T8c6qzc7iT2arXEG','0'), ('28','test31@test.test','$2y$10$WQ7.7pQWYKk/dkwOFLk36.lRCMQ0gC0ZDm.8Ou/OKkzkU21mNIayS','0'), ('29','test32@test.test','$2y$10$1NTltwwZK9QrSB9ZEyVpxeZ9wF/uzz87Z6zm84tX2FH85zjvAkvgS','0'), ('30','test33@test.test','$2y$10$eQHpZWQXNQBUVDeGIJfT/eisD1egI.y0uZ9fWzucWZbYN7e7y9NAW','1'), ('31','test34@test.test','$2y$10$fUzV3veN7je.EO/EeJrsLuKkAWMMd.QIrz2/kSqLALf1rJOK5cg.a','0'), ('32','test35@test.test','$2y$10$fg0Y.mH8Q2l5JjjDNxuvCeAPH.cill1DvXJO1tjSBR42wczUpOn9q','0'), ('33','test36@test.test','$2y$10$wd4djPB5RfuBTllEs7xwbeVnHywmnd0RH0LDFk2cZgGl54RkSigZG','0'), ('34','test37@test.test','$2y$10$VJ5vp5lXt9DdA2yDN1FgVucHefzVOW5jAiajueNzWvAAy6nIictVO','1'), ('35','test39@test.test','$2y$10$7xnqOGFeQLN1JopGf0i0veGiakeRy2lnifcG78yCNvJoCxCBxKRIK','1');
INSERT INTO `user_shop_map` VALUES ('1','1','1','0'), ('2','2','2','0'), ('3','3','3','0'), ('4','4','4','0'), ('5','5','5','0'), ('6','6','6','0'), ('7','7','7','0'), ('8','8','8','0'), ('9','9','9','0'), ('10','10','10','0'), ('11','11','11','0'), ('12','12','12','0'), ('13','13','13','0'), ('14','14','14','0'), ('15','15','15','0'), ('16','16','16','0'), ('17','17','16','0'), ('18','18','16','0'), ('19','19','16','0'), ('20','20','16','0'), ('21','21','16','0'), ('22','22','16','0'), ('23','23','23','0'), ('24','24','23','0'), ('25','25','23','0'), ('26','26','23','0'), ('27','27','27','0'), ('28','28','28','0'), ('29','29','29','0'), ('30','30','30','0'), ('31','31','31','0'), ('32','32','32','0'), ('33','33','33','0'), ('34','34','34','0'), ('35','35','35','0');
